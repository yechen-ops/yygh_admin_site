import request from '../../utils/request'

export default {
  // 获取数据字典
  getDictList(id) {
    return request({
      url: `/admin/cmn/dict/findChileData/${id}`,
      method: 'get'
    })
  },

  // 根据 dictCode 获取下级节点
  findByDictCode(dictCode) {
    return request({
      url: `/admin/cmn/dict/findByDictCode/${dictCode}`,
      method: 'get'
    })
  },

  // 根据 parentId 获取子数据
  findByParentId(parentId) {
    return request({
      url: `/admin/cmn/dict/findByParentId/${parentId}`,
      method: 'get'
    })
  }
}
