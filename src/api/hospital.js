import request from '../utils/request'

export default {
  //医院列表
  getPageList(current,limit,searchObj) {
    return request ({
      url: `/admin/hosp/hospital/page/${current}/${limit}`,
      method: 'get',
      params: searchObj
    })
  },

  // 更新医院的上线状态
  updateHospStatus(id, status) {
    return request ({
      url: `/admin/hosp/hospital/updateHospStatus/${id}/${status}`,
      method: 'put',
    })
  },

  // 查看医院详情
  getHospitalDetail(id) {
    return request ({
      url: `/admin/hosp/hospital/showHospitalDetail/${id}`,
      method: 'get',
    })
  },

  // 通过医院编号查询医院科室信息
  getDepartmentByHoscode(hoscode) {
    return request ({
      url: `/admin/hosp/department/getDeptList/${hoscode}`,
      method: 'get'
    })
  },

  // 获取排班规则
  getScheduleRule(page, limit, hoscode, depcode) {
    return request({
      url: `/admin/hosp/schedule/getScheduleRule/${page}/${limit}/${hoscode}/${depcode}`,
      method: 'get'
    })
  },

  //查询排班详情
  getDetailSchedule(hoscode,depcode,workDate) {
    return request ({
      url: `/admin/hosp/schedule/getDetailSchedule/${hoscode}/${depcode}/${workDate}`,
      method: 'get'
    })
  }


}
