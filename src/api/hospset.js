import request from '../utils/request'

export default {

  // 获取医院设置列表
  getHospSetList(current, limit, searchObj) {
    return request({
      url: `/admin/hosp/hospitalSet/page/${current}/${limit}`,
      method: 'post',
      data: searchObj // 使用 json 格式进行传递
    })
  },

  // 通过 id 删除医院设置
  removeHospSetById(id) {
    return request({
      url: `/admin/hosp/hospitalSet/remove/${id}`,
      method: 'delete'
    })
  },

  // 批量删除医院设置
  batchRemoveHospSet(idList) {
    return request({
      url: `/admin/hosp/hospitalSet/batchRemove`,
      method: 'delete',
      data: idList
    })
  },

  // 锁定和取消锁定
  lockHospitalSet(id, status) {
    return request({
      url: `/admin/hosp/hospitalSet/lock/${id}/${status}`,
      method: 'put',
    })
  },

  // 添加医院设置
  saveHospitalSet(hospitalSet) {
    return request({
      url: `/admin/hosp/hospitalSet/save`,
      method: 'post',
      data: hospitalSet
    })
  },

  // 根据 id 获取医院
  getHospitalSetById(id) {
    return request({
      url: `/admin/hosp/hospitalSet/getById/${id}`,
      method: 'get',
    })
  },

  // 修改医院设置
  updateHospitalSetById(hospitalSet) {
    return request({
      url: `/admin/hosp/hospitalSet/update`,
      method: 'put',
      data: hospitalSet
    })
  }
}
