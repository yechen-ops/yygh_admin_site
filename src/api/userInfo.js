import request from '../utils/request';

const api_url = '/admin/user';

export default {

    // 获取用户列表
    getPageList(page, limit, searchObj) {
        return request({
            url: `${api_url}/page/${page}/${limit}`,
            method: 'GET',
            params: searchObj
        })
    },

    // 更改用户锁定状态
    lockUser(id, status) {
        return request({
          url: `${api_url}/lock/${id}/${status}`,
          method: 'PUT',
        })
    },

    //用户详情
    show(id) {
      return request({
        url: `${api_url}/showInfo/${id}`,
        method: 'GET'
      })
    },

    //认证审批
    approval(id, authStatus) {
      return request({
        url: `${api_url}/approval/${id}/${authStatus}`,
        method: 'put'
      })
    }



}


